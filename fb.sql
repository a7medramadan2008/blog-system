-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 24, 2019 at 09:21 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fb`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(50) NOT NULL,
  `user` varchar(50) NOT NULL,
  `comment` text NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `post_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user`, `comment`, `time`, `post_id`) VALUES
(1, 'ahmed', 'this is nice post', '2019-10-23 19:57:07', 0),
(2, 'tarek', 'great project', '2019-10-23 19:57:29', 1),
(4, 'medo', ' yes we can', '2019-10-23 20:38:34', 2),
(5, 'merna', 'post system', '2019-10-23 20:41:39', 2),
(6, 'romio', 'engineering of software', '2019-10-23 20:42:14', 1),
(7, 'maze', 'ha ha ha ', '2019-10-23 20:58:44', 3),
(8, '123', 'romio', '2019-10-24 10:34:20', 12),
(9, 'moka', 'im developer', '2019-10-24 10:35:02', 12),
(10, '', 'yes i know', '2019-10-24 16:15:59', 11),
(11, '', 'nice work', '2019-10-24 16:54:44', 12),
(12, '', 'why ?\r\n', '2019-10-24 17:16:11', 11),
(13, 'romio122', 'koko', '2019-10-24 18:44:29', 14),
(14, 'romio122', 'im proud of my self', '2019-10-24 19:18:33', 1),
(15, 'ahmed', 'ahmed comment', '2019-10-24 19:19:21', 14);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(50) NOT NULL,
  `user` varchar(50) NOT NULL,
  `postText` text NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user`, `postText`, `time`) VALUES
(1, 'dad', 'hi', '2019-10-23 14:50:50'),
(2, 'ahmed', 'secound post', '2019-10-23 14:51:12'),
(3, 'medo', 'third post', '2019-10-23 14:51:30'),
(9, 'Ahmed Ramadan', 'hi im here', '2019-10-23 19:51:30'),
(10, 'Ahmed Ramadan', 'wellcom', '2019-10-23 20:18:30'),
(11, 'Ahmed Ramadan', 'romio', '2019-10-23 20:43:19'),
(12, 'Ahmed Ramadan', 'romio', '2019-10-24 10:20:33'),
(13, 'ahmed', 'session', '2019-10-24 18:38:21'),
(14, 'romio122', 'romio user post', '2019-10-24 18:39:51');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'ahmed', '123'),
(2, 'romio122', '123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
